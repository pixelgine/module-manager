@extends($layout)

@section('content-header')
	<h1>
		{{ ucwords($module->getName()) }} Module
		<small class="pull-right" style="margin-top: 8px;">
			{!! modal_popup(route('admin.modulemanager.destroy', $module->getName()), 'Delete module', trans('modulemanager::modules.modal.delete', ['name' => ucwords($module->getName())]))!!}
		</small>
		&middot;
		<small>
		@if ($module->enabled())
			{!! modal_popup(route('admin.modulemanager.disable', $module->get('name')), trans('modulemanager::modules.disable'), trans('modulemanager::modules.modal.disable', ['name' => $module->get('name')]))!!}
		@else
			{!! modal_popup(route('admin.modulemanager.enable',  $module->get('name')), trans('modulemanager::modules.enable'),  trans('modulemanager::modules.modal.enable',  ['name' => $module->get('name')]))!!}
		@endif
		</small>
		&middot;
		<small>
			{!! modal_popup(route('admin.modulemanager.migrate', $module->getName()), trans('modulemanager::modules.migrate'), trans('modulemanager::modules.modal.migrate', ['name' => ucwords($module->getName())]))!!}
		</small>
		&middot;
		<small>
			{!! modal_popup(route('admin.modulemanager.seed', $module->getName()), trans('modulemanager::modules.seed'), trans('modulemanager::modules.modal.seed', ['name' => ucwords($module->getName())]))!!}
		</small>
		&middot;
		<small>
			{!! modal_popup(route('admin.modulemanager.publish', $module->getName()), trans('modulemanager::modules.publish'), trans('modulemanager::modules.modal.publish', ['name' => ucwords($module->getName())]))!!}
		</small>
		&middot;
		<small>{!! link_to_route('admin.modulemanager.index', trans('modulemanager::modules.back')) !!}</small>
	</h1>
@stop

@section('content')
	{{--
	<div>
		@include('modulemanager::admin.form', array('model' => $module))
	</div>
	--}}
	<div>
		<table class="table">
			<thead>
				<tr>
					<th width="25%">{{ trans('modulemanager::modules.table.name') }}</th>
					<th width="55%">{{ trans('modulemanager::modules.table.path') }}</th>
					<th>{{ trans('modulemanager::modules.table.alias') }}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ ucwords($module->getName()) }}</td>
					<td>{{ $module->getPath() }}</td>
					<td>{{ $module->getAlias() }}</td>
				</tr>
			</tbody>
		</table>
		<h4>Composer information</h4>
		<table class="table">
			<thead>
			<tr>
				<th width="25%">{{ trans('modulemanager::modules.composer.package') }}</th>
				<th width="25%">{{ trans('modulemanager::modules.composer.autoload') }}</th>
				<th width="30%">{{ trans('modulemanager::modules.composer.require') }}</th>
				<th>{{ trans('modulemanager::modules.composer.owner') }}</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td class="nowrap">{{ $composer->name }}</td>
				<td>
				@forelse( $composer->autoload AS $loader => $namespaces )
					<strong>{{ $loader }}</strong><br>
					@foreach( $namespaces AS $name => $path )
						{{ $name . ' @/' . $path }}<br>
					@endforeach
				@empty
					Unknow
				@endforelse
				</td>
				<td>
				@if ( isset($composer->require) )
				<strong>require</strong><br>
				@forelse( $composer->require AS $package => $version )
					{{ $package . '@' . $version }}<br>
				@empty
					Unknow
				@endforelse
				@endif

				@if ( isset($composer->{"require-dev"}) )
					<strong>require-dev</strong><br>
					@forelse( $composer->{"require-dev"} AS $package => $version )
						{{ $package . '@' . $version }}<br>
					@empty
						Unknow
					@endforelse
				@endif
				</td>
				<td>
				@forelse( $composer->authors AS $author )
					{{ $author->name }} &lt;{{ $author->email }}&gt;<br>
				@empty
					Unknow
				@endforelse
				</td>
			</tr>
			</tbody>
		</table>


{{--
			<a href="{!! route('admin.modulemanager.install', $module->getName()) !!}" class="btn">Install module</a>
			--}}
	</div>
@stop
