<?php

return [
	'title'  => 'Modules',
	'modules'=> 'Modules',
	'module' => 'Module',
	'all' 	 => 'All Modules',
	'create' => 'Add New Module',
	'edit'	 => 'Edit',
	'enable' => 'Enable',
	'disable'=> 'Disable',
	'back'	 => 'Back',
	'migrate'=> 'Migrate',
	'seed'	 => 'Seed',
	'publish'=> 'Publish',
	'table'	 =>
	[
		'id'		=> 'no',
		'alias'		=> 'Alias',
		'description'=>'Description',
		'name'		=> 'Name',
		'status'	=> 'Status',
		'path'		=> 'Path',
		'action'	=> 'Action',
		'tags'		=> 'Tags',
	],
	'composer'	=>
	[
		'package'	=> 'Package',
		'owner'		=> 'Owner',
		'require'	=> 'Require',
		'autoload'	=> 'Autoload',
	],
	'modal'	=>
	[
		'enable'	=> 'Do you really enable <strong>:name</strong> module?',
		'disable'	=> 'Do you really disable <strong>:name</strong> module?',
		'delete'	=> 'Do you really delete <strong>:name</strong> module?',
	]
];
