<?php


if (Auth::check()) {
    try {
        Trusty::registerPermissions();
    } catch (PDOException $e) {
        //
    }
    Trusty::when(['admin/modules', 'admin/modules/*'], 'manage_modules');
}
