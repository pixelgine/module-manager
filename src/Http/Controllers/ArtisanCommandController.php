<?php namespace Pixelgine\ModuleManager\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Pingpong\Modules\Routing\Controller;
use Module;

class ArtisanCommandController extends Controller
{
	/**
	 * Migrating module
	 *
	 * @param $name
	 * @return mixed
	 */
	public function migrate( $name )
	{
		$module = Module::find( $name );

		if ( !$module )
			return Redirect::back()
				->withFlashMessage( $name. ' module not found!' )
				->withFlashType('danger');

		$run = Artisan::call('module:migrate', [
			$name
		]);

		if ( $run !== 0 )
		{
			return Redirect::back()
				->withFlashMessage( $module->getStudlyName(). ' module not migrated!' )
				->withFlashType('danger');
		}

		return Redirect::back()
			->withFlashMessage( $module->getStudlyName(). ' module migrated!' )
			->withFlashType('success');
	}

	/**
	 * Seeding module
	 *
	 * @param $name
	 * @return mixed
	 */
	public function seed( $name )
	{
		$module = Module::find( $name );

		if ( !$module )
			return Redirect::back()
				->withFlashMessage( $name. ' module not found!' )
				->withFlashType('danger');

		$run = Artisan::call('module:seed', [
			$name
		]);

		if ( $run !== 0 )
		{
			return Redirect::back()
				->withFlashMessage( $module->getStudlyName(). ' module not seeded!' )
				->withFlashType('danger');
		}

		return Redirect::back()
			->withFlashMessage( $module->getStudlyName(). ' module seeded!' )
			->withFlashType('success');
	}

	/**
	 * Puiblishing module
	 *
	 * @param $name
	 * @return mixed
	 */
	public function publish( $name )
	{
		$module = Module::find( $name );

		if ( !$module )
			return Redirect::back()
				->withFlashMessage( $name. ' module not found!' )
				->withFlashType('danger');

		$run = Artisan::call('module:publish', [
			$name
		]);

		if ( $run !== 0 )
		{
			return Redirect::back()
				->withFlashMessage( $module->getStudlyName(). ' module not published!' )
				->withFlashType('danger');
		}

		return Redirect::back()
			->withFlashMessage( $module->getStudlyName(). ' module published!' )
			->withFlashType('success');
	}

	/**
	 * Enable module
	 *
	 * @param $name
	 * @return mixed
	 */
	public function enable( $name )
	{
		$module = Module::find( $name );
		$module->enable();

		if ( $module->disabled() )
		{
			return Redirect::back()
				->withFlashMessage( $module->getStudlyName(). ' module not enabled!' )
				->withFlashType('danger');
		}

		return Redirect::back()
			->withFlashMessage( $module->getStudlyName(). ' module enabled!' )
			->withFlashType('success');
	}

	/**
	 * Disable module
	 *
	 * @param $name
	 * @return mixed
	 */
	public function disable( $name )
	{
		$module = Module::find( $name );
		$module->disable();

		if ( $module->enabled() )
		{
			return Redirect::back()
				->withFlashMessage( $module->getStudlyName(). ' module not disabled!' )
				->withFlashType('danger');
		}

		return Redirect::back()
			->withFlashMessage( $module->getStudlyName(). ' module disabled!' )
			->withFlashType('success');
	}


	/**
	 * #TODO
	 * Install module
	 *
	 * @param $name
	 * @return mixed
	 */
	public function install( $name )
	{
		$module = Module::install( $name );
		$name = ucwords($name);

		if ( !$module )
		{
			return Redirect::route('admin.modulemanager.edit', $name)
				->withFlashMessage( $name. ' module not installed!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.modulemanager.edit', $name)
			->withFlashMessage( $name. ' module installed!' )
			->withFlashType('success');
	}

	/**
	 * Destroy module
	 *
	 * @param $name
	 * @return mixed
	 */
	public function destroy( $name )
	{
		// delete module
		$module = Module::find( $name );
		$module->delete();

		// check module
		$module = Module::find( $name );
		$name = ucwords($name);

		if ( $module )
		{
			return Redirect::route('admin.modulemanager.index')
				->withFlashMessage( $name. ' module not deleted!' )
				->withFlashType('danger');
		}

		return Redirect::route('admin.modulemanager.index')
			->withFlashMessage( $name. ' module deleted!' )
			->withFlashType('success');
	}
}