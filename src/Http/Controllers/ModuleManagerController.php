<?php namespace Pixelgine\ModuleManager\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Pingpong\Modules\Routing\Controller;
use Module;

class ModuleManagerController extends Controller
{
	/**
	 * Listing modules
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$modules = $this->allOrSearch( Input::get('q') );

		return view('modulemanager::admin/index', compact('modules'));
	}

	/**
	 * Edit and show module details
	 *
	 * @param $name
	 * @return \Illuminate\View\View
	 */
	public function edit( $name )
	{
		$module = Module::find( $name );
		$composer = file_get_contents( realpath($module->getPath() . '/composer.json') );
		$composer = json_decode( $composer );

		return view('modulemanager::admin/edit', compact('module','composer'));
	}

	public function create()
	{

	}

	public function store()
	{
		/*Artisan::call('migrate', [
			'--package'=>'cartalyst/sentry'
		]);*/
		// http://laravel-tricks.com/tricks/run-artisan-commands-form-route-or-controller
	}

	/**
	 * Searching function for admin
	 *
	 * @param null $query
	 * @return array
	 */
	private function allOrSearch( $query = null )
	{
		$modules = Module::all();
		if ( $query )
		{
			$query = strtolower( $query );
			foreach ( $modules as $key => $module)
			{
				if( strpos( strtolower($module->getStudlyName()), $query ) === false
				&&  strpos( strtolower($module->getName()), $query ) 	   === false )
				{
					unset($modules[$key]);
				}

			}
		}

		return $modules;
	}
}