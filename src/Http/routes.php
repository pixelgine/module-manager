<?php

Route::group(['prefix' => config('admin.prefix', 'admin'), 'namespace' => 'Pixelgine\ModuleManager\Http\Controllers'], function()
{
	Route::group(['middleware' => array_merge(
		config('admin.filter.auth'),
		['\Pixelgine\ModuleManager\Http\Middleware\PermissionCheck']
	)], function ()
	{
		Route::resource('modules', 'ModuleManagerController', [
			'except' => ['show','destroy'],
			'names' => [
				'index' 	=> 'admin.modulemanager.index',
				'create' 	=> 'admin.modulemanager.create',
				'store' 	=> 'admin.modulemanager.store',
				'show' 		=> 'admin.modulemanager.show',
				'update' 	=> 'admin.modulemanager.update',
				'edit' 		=> 'admin.modulemanager.edit',
				'destroy' 	=> 'admin.modulemanager.destroy'
			],
		]);

		Route::get('modules/{name}/enable',  ['as' => 'admin.modulemanager.enable',  'uses' => 'ArtisanCommandController@enable']);
		Route::get('modules/{name}/disable', ['as' => 'admin.modulemanager.disable', 'uses' => 'ArtisanCommandController@disable']);
		Route::get('modules/{name}/install', ['as' => 'admin.modulemanager.install', 'uses' => 'ArtisanCommandController@install']);
		Route::get('modules/{name}/destroy', ['as' => 'admin.modulemanager.destroy', 'uses' => 'ArtisanCommandController@destroy']);
		Route::get('modules/{name}/migrate', ['as' => 'admin.modulemanager.migrate', 'uses' => 'ArtisanCommandController@migrate']);
		Route::get('modules/{name}/seed', 	 ['as' => 'admin.modulemanager.seed', 	 'uses' => 'ArtisanCommandController@seed']);
		Route::get('modules/{name}/publish', ['as' => 'admin.modulemanager.publish', 'uses' => 'ArtisanCommandController@publish']);
	});
});
