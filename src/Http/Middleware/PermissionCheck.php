<?php

namespace  Pixelgine\ModuleManager\Http\Middleware;

use Closure;
use Auth;
use Pingpong\Trusty\Exceptions\PermissionDeniedException;
use Trusty;

class PermissionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $permission = 'manage_modules';

        if ( Auth::user()->can($permission) )
            return $next($request);

        throw new PermissionDeniedException("You don't have permission to \"{$permission}\".");

    }
}
